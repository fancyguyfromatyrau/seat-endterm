import math

from django.test import TestCase
from django.urls import reverse, resolve
from .models import Transaction
from django.db import IntegrityError
import views
import constants


class TransactionClassTestCase(TestCase):

    def test_amount_for_decimal_places(self, amount):
        frac, whole = math.modf(amount)
        self.assertGreater(len(str(frac)), 3, "Cannot have more than 2 decimal places")

    def test_amount_for_max_digits(self, money):
        self.assertGreater(money, 9999999999.99, 'You cannot amount more!')

    def test_balance_after_transaction_for_decimal_places(self, amount):
        frac, whole = math.modf(amount)
        self.assertGreater(len(str(frac)), 3, "Cannot have more than 2 decimal places")

    def test_balance_after_transaction_amount_for_max_digits(self, money):
        self.assertGreater(money, 9999999999.99, 'You cannot have balance after transaction more!')

    def test_transaction_type(self, transaction_type):

        transaction_type = str(transaction_type).upper()
        deposit, withdrawal, interest = None, None, None

        if transaction_type == "Deposit".upper():
            deposit = 1
        elif transaction_type == "Withdrawal".upper():
             withdrawal = 2
        elif transaction_type == "interest".upper():
             interest = 3

        try:
            if deposit:
                self.assertEqual(deposit, constants.DEPOSIT)
            elif withdrawal:
                self.assertEqual(withdrawal, constants.WITHDRAWAL)
            elif interest:
                self.assertEqual(interest, constants.INTEREST)
        except Exception as transaction_type_error:
            raise ValueError("Select deposit, withdrawal or interest")


class URLTestCase(TestCase):
    def test_deposit_url(self):
        url = reverse('deposit')
        self.assertEquals(resolve(url).func.view_class, views.DepositMoneyView.as_view())

    def test_logout_url(self):
        url = reverse('report')
        self.assertEquals(resolve(url).func.view_class, views.TransactionRepostView.as_view())

    def test_register_url(self):
        url = reverse('withdraw')
        self.assertEquals(resolve(url).func.view_class, views.WithdrawMoneyView.as_view())



