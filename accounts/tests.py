from decimal import Decimal

from django.test import TestCase
from django.urls import reverse, resolve

from .models import User, BankAccountType, UserBankAccount, UserAddress
from django.db import IntegrityError
import math
from constants import MALE, FEMALE
import views
import urls


# Create your tests here.


class UserClassTestCase(TestCase):
    """Initialize User"""

    def setUp(self):

        # create first user with email and password
        self.user = User.objects.create_user(email='test1@aitu.kz')
        self.user.set_password('nonadmin')
        self.user.save()

        # create second user with email and password
        self.user = User.objects.create_user(email='test2@aitu.kz')
        self.user.set_password('nonadmin')
        self.user.save()

        # create third user with email and password
        self.user = User.objects.create_user(email='test3@aitu.kz')
        self.user.set_password('nonadmin')
        self.user.save()

    """ Testing got  """

    def test_for_existing_email(self):
        test_email = 'test1@aitu.kz'
        test_email_array = User.objects.values_list('email')
        for email in test_email_array:
            if test_email == email:
                self.assertEqual(test_email == email, "This email is already used!")

    def test_for_unique_email(self):
        test_email = 'test@aitu.kz'
        if test_email == self.user.email:
            raise IntegrityError("Email is not unique, change it!")
        else:
            print("OK")

    def test_username_field(self):
        username_field = "email"
        test_user = User()
        self.assertEqual(username_field, test_user.USERNAME_FIELD)


class BankAccountTypeTestCase(TestCase):
    """Testing the entrance of more money than allowed"""

    def test_for_withdraw_max_digits(self, money):
        self.assertGreater(money, 9999999999.99, 'You cannot withdraw more')

    """Testing for the maximum decimal places"""

    def test_withdraw_for_decimal_places(self, money):
        frac, whole = math.modf(money)
        self.assertGreater(len(str(frac)), 3, "Cannot have more than 2 decimal places")

    """Testing for the positive value of interest calculation per year"""

    def test_for_positive_interest_calculation_per_year(self, interest_level):
        self.assertLess(interest_level, 0, "Interest calculation per year must be positive")

    def test_calculation_of_interest(self, principal):
        p = 100
        r = 0.2
        n = 2
        interest = round((p * (1 + ((r / 100) / n))) - p, 2)
        self.assertEqual(interest, 0.1)


class UserBankAccountTestCase(TestCase):
    """ User has to enter either male or female """

    def test_for_valid_gender(self, gender):
        gender = str(gender).upper()
        male, female = None, None
        if gender == "MALE":
            male = 'M'
        elif gender == "FEMALE":
            female = 'F'
        try:
            if male:
                self.assertEqual(male, MALE)
            elif female:
                self.assertEqual(female, FEMALE)
        except Exception as gender_error:
            raise ValueError("Select either male or female")


class UserRegistrationViewTestCase(TestCase):
    template = "accounts/user_registration.html"

    """Testing the transfer to the right page"""

    def test_user_registration_view_page(self):
        self.client.login()
        response = self.client.get('')
        self.assertTemplateUsed(response, 'accounts/user_registration.html')


class UserLoginViewTestCase(TestCase):
    def test_user_login_view_page(self):
        response = self.client.get('accounts/user_login.html')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template_name='accounts/user_login.html')


class UserAddressTestCase(TestCase):
    def test_postal_code(self, postal_code):
        self.assertLess(postal_code, 0, "Postal code cannot be negative!")

    def test_length_of_city(self, city):
        self.assertGreater(len(str(city)), 256, "No such city")

    def test_length_of_street_address(self, street_address):
        self.assertGreater(len(str(street_address)), 256, "No such street address")

    def test_length_of_country(self, country):
        self.assertGreater(len(str(country)), 256, "No such country")


class URLTestCase(TestCase):

    def test_login_url(self):
        url = reverse('login')
        self.assertEquals(resolve(url).func.view_class, views.UserLoginView.as_view())

    def test_logout_url(self):
        url = reverse('logout')
        self.assertEquals(resolve(url).func.view_class, views.LogoutView.as_view())

    def test_register_url(self):
        url = reverse('register')
        self.assertEquals(resolve(url).func.view_class, views.UserRegistrationView.as_view())


